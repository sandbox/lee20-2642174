<?php

/**
 * @file
 * Migrate support for machine fields.
 */

/**
 * Implementation of hook_migrate_api().
 */
function machine_migrate_api() {
  return array(
    'api' => 2,
    'field handlers' => array(
      'MigrateMachineWithTextFieldHandler',
    ),
  );
}

/**
 * Migrate field handler for machine_with_text fields.
 *
 * Option #1: Prepared
 *   Handy for migrating identical structures with JSON when the data is
 *   already as is needed.
 *   Use prepareRow() to structure the data as follows when necessary:
 * @code
 *   // A practical example of prepareRow()
 *   list($text, $machine) = explode(':', $row->source_field);
 *   $row->source_field = array(
 *     'text' => $text,
 *     'machine' => $machine,
 *   );
 *   // That can be mapped as:
 *   $this->addFieldMapping('field_destination', 'source_field');
 *   $this->addFieldMapping('field_destination:text', NULL);
 *
 *   // Multiple prepared values should look like:
 *   $row->source_field = array(
 *     array('machine' => 'machine1', 'text' => 'Machine #1'),
 *     array('machine' => 'machine2', 'text' => 'Machine #2'),
 *    );
 *
 *   // Mapping is identical for both single and multiple prepared values
 *   $this->addFieldMapping('field_destination', 'source_field')
 *   $this->addFieldMapping('field_destination:text', NULL);
 * @endcode
 *
 * Option #2: Text Subfield
 *   This method is untested but is closely modeled after similar fields.
 *   If you encounter issues, please post to the issue queue.
 * @code
 *   // Map the machine name to the field and the text to the 'text' subfield.
 *   $this->addFieldMapping('field_destination', 'machine_source');
 *   $this->addFieldMapping('field_destination:text', 'machine_text_source');
 *   // Use a separator for multiple values
 *   $this->addFieldMapping('field_destination', 'machine_source')
 *      ->separator(',');
 *   $this->addFieldMapping('field_destination:text', 'machine_source')
 *      ->separator(',');
 * @endcode
 */
class MigrateMachineWithTextFieldHandler extends MigrateFieldHandler {
  public function __construct() {
    $this->registerTypes(array('machine_with_text'));
  }

  public function fields($type) {
    return array(
      'text' => t('Subfield: Human readable text'),
    );
  }

  public function prepare($entity, array $field_info, array $instance, array $values) {
    $arguments = array();
    if (isset($values['arguments'])) {
      $arguments = array_filter($values['arguments']);
      unset($values['arguments']);
    }
    $language = $this->getFieldLanguage($entity, $field_info, $arguments);
    $delta = 0;
    foreach ($values AS $value) {
      if (isset($arguments['text'])) {
        // Text subfield is mapped
        $return[$language][$delta]['machine'] = $value;
        if (isset($arguments['text'])) {
          if (is_array($arguments['text'])) {
            $return[$language][$delta]['text'] = $arguments['text'][$delta];
          }
          else {
            $return[$language][$delta]['text'] = $arguments['text'];
          }
        }
      }
      else {
        // Text subfield not mapped, assume prepared values
        $return[$language][$delta] = (array)$value;
      }
      $delta++;
    }
    return isset($return) ? $return : NULL;
  }
}
